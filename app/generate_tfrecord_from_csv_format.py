"""
Holds the functionality of conversion from csv annotation format to Tf-record


Credits:
    Generate Tf-record core :
    https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md

    Added:
        * Extraction of the relevant annotation information *.csv annotation file

Usage:
  # From tensorflow/models/
  # Create train data:
  python generate_tfrecord.py --json_input=data/train_labels.csv  --output_path=train.record --data_path=data/train/
  # Create test data:
  python generate_tfrecord.py --json_input=data/test_labels.csv  --output_path=test.record --data_path=data/test/

"""

import os
import pandas as pd
import tensorflow as tf

from PIL import Image
from models.research.object_detection.utils import dataset_util


flags = tf.app.flags
flags.DEFINE_string('annotation_file', '', 'Path to the csv annotation')
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
flags.DEFINE_string('data_path', '', 'Path to data')

FLAGS = flags.FLAGS


def load_image(image_path):
    with open(image_path, "rb") as imageFile:
        encoded_image_data = imageFile.read()
    width, height = Image.open(image_path).size
    return encoded_image_data, height, width


def list_images_in_path(input_path):
    image_paths = []
    for file in os.listdir(input_path):
        if file.endswith(".jpg") or file.endswith(".png") or file.endswith(".JPG"):
            image_paths.append(os.path.join(input_path, file))
    return image_paths


def get_image_annotation(image_path, annotations):
    return annotations.loc[annotations.orig_filename == os.path.basename(image_path)]


def create_tf_example(image_path, image_annotation):

    encoded_image_data, image_height, image_width = load_image(image_path)
    filename = os.path.basename(image_path).encode('utf8')
    image_format = os.path.basename(image_path).split('.')[1].encode()
    xmins = (image_annotation.xmin / image_width).values.tolist()
    xmaxs = (image_annotation.xmax / image_width).values.tolist()
    ymins = (image_annotation.ymin / image_height).values.tolist()
    ymaxs = (image_annotation.ymax / image_height).values.tolist()
    classes_text = [image_class.encode() for image_class in list(image_annotation.type)]
    classes = list(image_annotation.type.astype('str').replace(('car', 'special_car'), (1, 2)))

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(image_height),
        'image/width': dataset_util.int64_feature(image_width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_image_data),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))

    return tf_example


def generate_tfrecord_from_csv(FLAGS):
    writer = tf.python_io.TFRecordWriter(FLAGS.output_path)

    annotation = pd.read_csv(FLAGS.annotation_file)
    image_paths = list_images_in_path(FLAGS.data_path)

    for image_path in image_paths:
        image_annotation = get_image_annotation(image_path, annotation)
        tf_example = create_tf_example(image_path, image_annotation)
        writer.write(tf_example.SerializeToString())

    writer.close()


# if __name__ == '__generate_tfrecord_from_csv__':
#     tf.app.run()


if __name__ == '__main__':
    generate_tfrecord_from_csv(FLAGS)
